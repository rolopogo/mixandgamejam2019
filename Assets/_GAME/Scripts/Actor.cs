﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Xero.Variables;

[ExecuteInEditMode]
[DisallowMultipleComponent]
[RequireComponent(typeof(Moveable))]
public class Actor : MonoBehaviour
{
    [HideInInspector] public Moveable move;

    public void Awake()
    {
        move = gameObject.EnsureComponent<Moveable>();
    }

    public void Destroy()
    {
        gameObject.SafeDestroy();
    }
}
