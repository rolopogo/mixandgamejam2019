﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class AudioFFT : MonoBehaviour
{
    public float smoothFactor;

    private AudioSource aud;
    public float[] _samples = new float[512];
    public float[] _rawBands = new float[8];
    public float[] _normalizedBands = new float[8];
    public float[] _smoothedBands = new float[8];
    float[] _maxBands = new float[8];

    // Start is called before the first frame update
    void Start()
    {
        aud = gameObject.EnsureComponent<AudioSource>();
        for (int i = 0; i < 8; i++)
        {
            _maxBands[i] = float.Epsilon;
        }
    }

    // Update is called once per frame
    void Update()
    {
        //get samples
        aud.GetSpectrumData(_samples, 0, FFTWindow.Blackman);


        // process into bands
        int sampleCount = 0;

        for (int i = 0; i < 8; i++)
        {
            float avg = 0;
            int bandSamples = (int)Mathf.Pow(2, i + 1);

            for (int j = 0; j < bandSamples; j++)
            {
                avg += _samples[sampleCount];
                sampleCount++;
            }

            avg /= bandSamples;

            _rawBands[i] = avg;
            
            _maxBands[i] = Mathf.Max(_maxBands[i], _rawBands[i]);
            _normalizedBands[i] = avg / _maxBands[i];
            _smoothedBands[i] = Mathf.Max(Mathf.Lerp(_smoothedBands[i], _normalizedBands[i], Time.deltaTime * smoothFactor), _normalizedBands[i]);
        }
    }
}
