﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Xero.Variables;

[RequireComponent(typeof(Camera))]
public class CameraBeatEffect : MonoBehaviour
{
    public AudioFFT fft;
    public IntRef band = (Ref)1;
    public FloatRef minPos = (Ref)0.2f;
    public FloatRef maxPos = (Ref)0.3f;
    public FloatRef minFov = (Ref)120f;
    public FloatRef maxFov = (Ref)140f;
    Camera camera;

    private void Awake()
    {
        camera = gameObject.EnsureComponent<Camera>();
    }

    private void Update()
    {
        float effectAmount = fft._smoothedBands[band];
        transform.localPosition = Vector3.forward * Mathf.Lerp(minPos, maxPos, effectAmount);
        camera.fieldOfView = Mathf.Lerp(minFov, maxFov, effectAmount);
    }
}