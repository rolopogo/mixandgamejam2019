﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class FadeRect : MonoBehaviour
{
    public bool fadeInOnAwake = true;
    public Image image;
    public Color inColor = Color.clear;
    public Color outColor = Color.black;

    public delegate void FadeCompleteCallback();

    private void Start()
    {
        if (fadeInOnAwake) FadeIn(1, null);
    }

    public void FadeIn(float t, FadeCompleteCallback callback)
    {
        StopAllCoroutines();
        StartCoroutine(FadeCR(t, outColor, inColor, callback));
    }

    public void FadeOut(float t, FadeCompleteCallback callback)
    {
        StopAllCoroutines();
        StartCoroutine(FadeCR(t, inColor, outColor, callback));
    }

    public IEnumerator FadeCR(float t, Color from, Color to, FadeCompleteCallback callback )
    {
        float timeElapsed = 0;
        while (timeElapsed < t)
        {
            timeElapsed += Time.unscaledDeltaTime;
            image.color = Color.Lerp(from, to, timeElapsed / t);
            yield return null;
        }
        callback?.Invoke();
    }
}