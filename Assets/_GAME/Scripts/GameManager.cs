﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public Player player;
    public PlayerMovement playerMovement;
    public Timer timer;
    public Canvas gameCanvas;
    public Canvas endScreen;

    public Spawner spawner;
    public FadeRect fadeRect;

    public AudioSource aud;

    private void Awake()
    {
        gameCanvas.gameObject.SetActive(true);
        endScreen.gameObject.SetActive(false);

        player.playerDidDieEvent += HandlePlayerDied;
        Time.timeScale = 1f;
        
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Exit();
        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            Restart();
        }

        if (aud.time >= aud.clip.length - 5) HandlePlayerWon();
        if (aud.time >= aud.clip.length - 10) spawner.Stop();
    }

    void HandlePlayerDied()
    {
        HandlePlayerWon();
        StartCoroutine(PitchBend());
    }

    public IEnumerator PitchBend()
    {
        float timeElapsed = 0;
        float duration = 2f;
        while (timeElapsed < duration)
        {
            timeElapsed += Time.unscaledDeltaTime;
            aud.pitch = Mathf.Lerp(1, 0.5f, timeElapsed/duration);
            aud.volume = Mathf.Lerp(1, 0f, timeElapsed / duration);
            Time.timeScale = Mathf.Lerp(1, .01f, timeElapsed / duration);
            yield return null;
        }
    }

    void HandlePlayerWon()
    {
        spawner.Stop();
        timer.Stop();
        endScreen.gameObject.SetActive(true);
        //gameCanvas.gameObject.SetActive(false);
        playerMovement.enabled = false;
    }

    public void Restart()
    {
        fadeRect.FadeOut(1, delegate() { SceneManager.LoadScene(SceneManager.GetActiveScene().name); });
    }

    public void Exit()
    {
        fadeRect.FadeOut(1, delegate () { SceneManager.LoadScene("MainMenu"); });
    }
}
