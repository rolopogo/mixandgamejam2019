﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Xero.Variables;

[ExecuteInEditMode]
[RequireComponent(typeof(Moveable))]
public class InputControls : MonoBehaviour
{
    public bool useUnityAxes;

    public bool cameraRelative;
    public Camera camera;

    private Moveable _moveable;
    public Moveable moveable
    {
        get
        {
            if(_moveable == null)
                _moveable = gameObject.EnsureComponent<Moveable>();

            return _moveable;
        }
    }

    private Vector2 _inputDir;
    public Vector2 InputDir
    {
        get
        {
            _inputDir.x = Input.GetAxisRaw("Horizontal");
            _inputDir.y = Input.GetAxisRaw("Vertical");

            return _inputDir;
        }
    }

    public KeyCodeRef forward = (Ref) KeyCode.W;
    public KeyCodeRef back    = (Ref) KeyCode.S;
    public KeyCodeRef right   = (Ref) KeyCode.D;
    public KeyCodeRef left    = (Ref) KeyCode.A;

    private void Update()
    {
        if (!Application.isPlaying) return;
        if (cameraRelative && camera == null) return;

        var moveDir = Vector3.zero;

        if (useUnityAxes)
        {
            var input = InputDir;
            moveDir.x = input.x;
            moveDir.z = input.y;
        }
        else
        {
            if (forward .Value.KeyHeld()) moveDir += Vector3.forward;
            if (back    .Value.KeyHeld()) moveDir += Vector3.back;
            if (right   .Value.KeyHeld()) moveDir += Vector3.right;
            if (left    .Value.KeyHeld()) moveDir += Vector3.left;
        }

        if (cameraRelative)
        {
            moveDir = camera.transform.TransformDirection(moveDir);
            moveDir.y = 0.0f;
        }

        moveDir.Normalize();
        moveable.targetDirection = moveDir;
    }
}
