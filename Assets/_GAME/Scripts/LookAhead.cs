﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Xero.Variables;

public class LookAhead : MonoBehaviour
{
    public PlayerMovement playerMovement;
    public FloatRef maxRot = (Ref)30f;

    private void Update()
    {
        transform.localRotation = Quaternion.Euler(playerMovement.velocity.x / playerMovement.speed * maxRot, playerMovement.velocity.y / playerMovement.speed * maxRot, 0);
    }
}
