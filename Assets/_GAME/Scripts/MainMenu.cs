﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    public FadeRect fadeRect;

    private void Awake()
    {
        Time.timeScale = 1f;
    }
    
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Exit();
        }
    }

    public void Song1()
    {
        fadeRect.FadeOut(1, delegate () { SceneManager.LoadScene("Song1"); });
    }

    public void Song2()
    {
        fadeRect.FadeOut(1, delegate() { SceneManager.LoadScene("Song2"); });
        
    }

    public void Song3()
    {
        fadeRect.FadeOut(1, delegate () { SceneManager.LoadScene("Song3"); });

    }

    public void Exit()
    {
        fadeRect.FadeOut(1, delegate() { Application.Quit(); });
    }
}
