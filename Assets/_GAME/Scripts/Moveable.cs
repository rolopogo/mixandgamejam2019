﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Xero.Variables;

[DisallowMultipleComponent]
public class Moveable : MonoBehaviour
{
    public Vector3 targetDirection;

    public FloatRef speed        = (Ref) 9.8f;
    public FloatRef friction     = (Ref) 9.8f;
    public FloatRef acceleration = (Ref) 9.8f;

    public Vector3  velocity = Vector3.zero;
    public float    vMagnitude   = 0.0f;

    private Transform _cachedTransform;
    public new Transform transform
    {
        get
        {
            if(_cachedTransform == null)
                _cachedTransform = gameObject.EnsureComponent<Transform>();

            return _cachedTransform;
        }
    }

    public void MoveTo(Vector3 position)
    {

    }

    private void Simulate(float dt)
    {
        //if (targetDirection.IsZero()) return;

        // Friction
        var drop = (1 - friction * dt);
        velocity *= drop;

        // Acceleration
        velocity += targetDirection * speed * acceleration * dt;

        vMagnitude = velocity.magnitude;

        // Neatly round velocity at extremes
        if (vMagnitude.AtMost(0.01f))
        {
            velocity = Vector3.zero;
            return;
        }

        if ((speed - vMagnitude).AtMost(0.01f))
            velocity = velocity.normalized * speed;

        transform.position += velocity * dt;
    }

    public void DoUpdate()
    {
        Simulate(Time.deltaTime);
    }

    private void OnEnable()
    {
        MoveableUpdateRunner.Register(this);
    }

    private void OnDisable()
    {
        MoveableUpdateRunner.Deregister(this);
    }
}
