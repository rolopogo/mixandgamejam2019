﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveableUpdateRunner : Singleton<MoveableUpdateRunner>
{
    private static List<Moveable> moveables = new List<Moveable>();

    public static void Register(Moveable m)
    {
        moveables.SafeAdd(m);
    }

    public static void Deregister(Moveable m)
    {
        moveables.SafeRemove(m);
    }

    private void Update()
    {
        for(int i = 0; i < moveables.Count; ++i)
        {
            moveables[i].DoUpdate();
        }
    }
}
