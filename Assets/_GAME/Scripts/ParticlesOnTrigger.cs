﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticlesOnTrigger : MonoBehaviour
{
    public ParticleSystem[] particles;
    
    private void OnTriggerEnter(Collider other)
    {
        foreach (ParticleSystem particle in particles)
        {
            particle.Play();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        foreach (ParticleSystem particle in particles)
        {
            particle.Stop();
        }
    }
}
