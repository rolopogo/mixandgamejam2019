﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public Action playerDidDieEvent;
    public GameObject playerModel;
    public ParticleSystem deathParticles;

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Wall")
        {
            playerDidDieEvent?.Invoke();
            playerModel.SetActive(false);
            deathParticles.Play();
        }
    }
}
