﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Xero.Variables;

public class PlayerMovement : MonoBehaviour {

    public FloatRef speed = (Ref)9.8f;
    public FloatRef friction = (Ref)9.8f;
    public FloatRef acceleration = (Ref)9.8f;
    public FloatRef maxShipRot = (Ref)60f;

    public Vector2 velocity = Vector3.zero;
    public float vMagnitude = 0.0f;

    Vector2 _moveDir;
    Vector2 _inputDir;

    Vector2 rotSpeed;
    public bool invertedY;
    public bool invertedX;

    public Vector2 InputDir
    {
        get
        {
            _inputDir.x = Input.GetAxisRaw("Horizontal");
            _inputDir.y = Input.GetAxisRaw("Vertical");

            return _inputDir;
        }
    }

    // Update is called once per frame
    void Update () {
        var _inputDir = InputDir.normalized;
        // rotate coordinates
        _moveDir.x = invertedY ? _inputDir.y : -_inputDir.y;
        _moveDir.y = invertedX ? -_inputDir.x : _inputDir.x;
        Simulate(Time.deltaTime);
    }

    private void Simulate(float dt)
    {
        //if (targetDirection.IsZero()) return;

        // Friction
        var drop = (1 - friction * dt);
        velocity *= drop;

        // Acceleration
        velocity += _moveDir * speed * acceleration * dt;

        vMagnitude = velocity.magnitude;

        // Neatly round velocity at extremes
        if (vMagnitude.AtMost(0.01f))
        {
            velocity = Vector3.zero;
            return;
        }

        if ((speed - vMagnitude).AtMost(0.01f))
            velocity = velocity.normalized * speed;

        transform.Rotate(velocity * dt);
        transform.GetChild(0).transform.localRotation = Quaternion.Euler(velocity.x / speed * maxShipRot, velocity.y / speed * maxShipRot, 0);
    }
}
