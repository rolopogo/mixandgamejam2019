﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RainbowController : MonoBehaviour
{
    public Material m;
    public Material m2;
    public bool rainbow;
    public float speed;
    public Gradient gradient;
    Color c;
    float hue;

    void Update()
    {
        if (rainbow)
        {
            hue = (Time.time / speed) % 1;
            c = Color.HSVToRGB(hue, 0.8f, 0.8f);
            m.color = c *0.3f;
            m2.SetColor("_EmissionColor", c);

            hue = ((Time.time + .5f) / speed) % 1;
            c = Color.HSVToRGB(hue, 0.8f, 0.8f);
            RenderSettings.fogColor = c;
        } else
        {
            c = gradient.Evaluate(Mathf.Sin(Time.time / speed));
            m2.SetColor("_EmissionColor", c);
            m.color = c * 0.3f;

            c = gradient.Evaluate(Mathf.Sin((Time.time + 0.5f) / speed));
            RenderSettings.fogColor = c;
        }
    }
}
