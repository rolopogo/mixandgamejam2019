﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateDynamic : MonoBehaviour
{
    public Vector3 axis;
    public float spinSpeed = 10;
    public float spinFrequency = 0.1f;

    private void Update()
    {
        transform.localRotation = Quaternion.Euler(axis * spinSpeed * Mathf.Sin(Time.time * spinFrequency));
    }
}
