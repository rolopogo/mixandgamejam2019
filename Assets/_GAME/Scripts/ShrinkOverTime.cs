﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShrinkOverTime : MonoBehaviour
{
    public float shrinkDuration = 2f;
    public Vector3 startScale;
    float time;

    // Start is called before the first frame update
    void Start()
    {
        startScale = transform.localScale;
    }

    // Update is called once per frame
    void Update()
    {
        time += Time.deltaTime;
        if (time > shrinkDuration) Destroy(gameObject);
        transform.localScale = Vector3.Lerp(startScale, Vector3.zero, time / shrinkDuration);
    }
}
