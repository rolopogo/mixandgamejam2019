﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class Singleton : MonoBehaviour
{
    [RuntimeInitializeOnLoadMethod]
    private static void RuntimeInit()
    {
        Type root = typeof(Singleton);

        var singletons = root.Assembly.GetTypes()
            .Where(type => type.IsSubclassOf(root) && !type.ContainsGenericParameters)
            .ToList();

        foreach(var s in singletons)
        {
            var init = s.GetMethod("InstanceInit", BindingFlags.NonPublic | BindingFlags.Static | BindingFlags.FlattenHierarchy);
            init.Invoke(null, null);
        }

        Debug.Log("Singletons enabled!");
    }
}

public class Singleton<T> : Singleton where T : MonoBehaviour
{
    private static readonly string _instanceName = $"Singleton.{typeof(T).Name}";
    private static GameObject _instanceObject;
    private static T _instance;

    public static T instance
    {
        get
        {
            if (_instance != null) return _instance;

            InstanceInit();

            return _instance;
        }
    }

    protected static void InstanceInit()
    {
        if (_instance != null) return;

        if (_instanceObject == null)
            _instanceObject = GameObject.Find(_instanceName);

        if (_instanceObject == null)
        {
            _instanceObject = new GameObject(_instanceName);
            GameObject.DontDestroyOnLoad(_instanceObject);
        }

        _instance = _instanceObject.EnsureComponent<T>();
    }
}
