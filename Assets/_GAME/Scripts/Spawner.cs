﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public enum ObstacleType { Tetra, Cube, Dodeca };
    bool spawning = true;

    [System.Serializable]
    public struct ObstacleData
    {
        public ObstacleType obstacleType;
        public Mesh mesh;
        public Transform[] permutations;
    }

    public ObstacleType obstacleType = ObstacleType.Cube;

    public ObstacleData[] obstacles;
    public Dictionary<ObstacleType, ObstacleData> obstacleDict;

    public float spawnScale = 10;
    public float spawnDelay = 1f;
    public Transform prefab;
    float timeSinceLastSpawn = 0;

    private void Awake()
    {
        obstacleDict = new Dictionary<ObstacleType, ObstacleData>();
        foreach (ObstacleData item in obstacles)
        {
            obstacleDict.Add(item.obstacleType, item);
        }
    }

    private void Update()
    {
        if (!spawning) return;
        timeSinceLastSpawn += Time.deltaTime;
        if (timeSinceLastSpawn > spawnDelay)
        {
            // spawn
            timeSinceLastSpawn = 0;

            // pick a permutation
            ObstacleData data = obstacleDict[obstacleType];
            Transform prefab = data.permutations[Mathf.RoundToInt(Random.value * (data.permutations.Length-1))];
            Vector3 rot = data.mesh.vertices[Mathf.RoundToInt(Mathf.Clamp01(Mathf.PerlinNoise(1, Time.timeSinceLevelLoad)) * (data.mesh.vertices.Length - 1))];
            Transform clone = Instantiate(prefab, Vector3.zero, Quaternion.LookRotation(rot, Vector3.up), transform);
            clone.localScale = Vector3.one * spawnScale;
            clone.tag = "Wall";
        }
    }

    public void UseObstacleType(ObstacleType type)
    {
        obstacleType = type;
    }

    public void Stop()
    {
        spawning = false;
    }
}

