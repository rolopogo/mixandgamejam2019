﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Timer : MonoBehaviour
{
    TextMeshProUGUI timerText;
    bool running = true;
    float time = 0;
    // Start is called before the first frame update
    void Awake()
    {
        timerText = GetComponent<TextMeshProUGUI>();
    }

    // Update is called once per frame
    void Update()
    {
        if (running) {
            time += Time.deltaTime;
        }

        var seconds = Mathf.FloorToInt(time) % 60; // calculate the seconds
        var minutes = Mathf.FloorToInt(time / 60); // calculate the minutes
        var ms = 100*(time - Mathf.Floor(time));

        timerText.text = String.Format("<size=60%>{0:0}:<size=100%>{1:00}:<size=40%>{2:00}", minutes, seconds,ms);
    }

    public void Stop()
    {
        running = false;
    }
}
