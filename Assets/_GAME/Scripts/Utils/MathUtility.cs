﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public static class MathUtility
{
    public static float Cubed(this float f)
    {
        return f * f * f;
    }

    public static float Squared(this float f)
    {
        return f * f;
    }

    public static float Dist(this float a, float f)
    {
        var dist = f - a;

        if (dist < 0)
            dist *= -1;

        return dist;
    }

    public static int NearestPerfectSquare(this int i)
    {
        return (int)((float)i).NearestPerfectSquare();
    }

    public static int PreviousPerfectSquare(this int i)
    {
        return (int)((float)i).PreviousPerfectSquare();
    }

    public static float PreviousPerfectSquare(this float f)
    {
        var root = Mathf.Sqrt(f);
        var prev = (float)Mathf.FloorToInt(root) - 1;

        return prev.Squared();
    }

    public static float NearestPerfectSquare(this float f)
    {
        var root = Mathf.Sqrt(f);

        if (root % 1 == 0) return f;

        var prev = (float)Mathf.FloorToInt(root);
        var next = (float)Mathf.CeilToInt(root);

        var sqPrev = prev.Squared();
        var sqNext = next.Squared();

        var dPrev = f.Dist(sqPrev);
        var dNext = f.Dist(sqNext);

        return (int)(dPrev < dNext ? sqPrev : sqNext);
    }
}
