﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public static class MeshUtility
{
    public static Mesh GenerateGridPlane(int resolution, float scale = 1.0f)
    {
        Mesh mesh = new Mesh();

        mesh.indexFormat = UnityEngine.Rendering.IndexFormat.UInt32;

        mesh.name = "Procedural Grid Plane";

        resolution = (resolution * 100).PreviousPerfectSquare();

        int xSize = (int)Mathf.Sqrt(resolution);
        int ySize = xSize;

        Vector3[] vertices = new Vector3[(xSize + 1) * (ySize + 1)];

        int offset = xSize / 2;

        for (int i = 0, y = 0; y <= ySize; ++y)
        {
            for (int x = 0; x <= xSize; x++, ++i)
            {
                vertices[i] = new Vector3((x - offset) * scale , 0, (y - offset) * scale);
            }
        }

        mesh.vertices = vertices;

        int[] triangles = new int[xSize * ySize * 6];

        for (int ti = 0, vi = 0, y = 0; y < ySize; y++, vi++)
        {
            for (int x = 0; x < xSize; x++, ti += 6, vi++)
            {
                triangles[ti] = vi;
                triangles[ti + 3] = triangles[ti + 2] = vi + 1;
                triangles[ti + 4] = triangles[ti + 1] = vi + xSize + 1;
                triangles[ti + 5] = vi + xSize + 2;
            }
        }

        mesh.triangles = triangles;

        mesh.RecalculateNormals();
        mesh.Optimize();

        return mesh;
    }
}
