﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class UnityUtility
{
    public static void SafeDestroy(this Object obj)
    {
        if (Application.isEditor && !Application.isPlaying)
            Object.DestroyImmediate(obj);
        else
            Object.Destroy(obj);
    }

    public static T EnsureComponent<T>(this Component component) where T : Component
    {
        return component.gameObject.EnsureComponent<T>();
    }

    public static T EnsureComponent<T>(this GameObject obj) where T : Component
    {
        var comp = obj.GetComponent<T>();

        if (comp != null) return comp;

        comp = obj.AddComponent<T>();

        return comp;
    }

    public static bool AtMost(this float a, float b)
    {
        return a < b || a.Nearly(b);
    }

    public static bool Nearly(this float a, float b)
    {
        return Mathf.Approximately(a, b);
    }

    public static bool KeyUp(this KeyCode keyCode)
    {
        return Input.GetKeyUp(keyCode);
    }

    public static bool KeyHeld(this KeyCode keyCode)
    {
        return Input.GetKey(keyCode);
    }

    public static bool KeyDown(this KeyCode keyCode)
    {
        return Input.GetKeyDown(keyCode);
    }

    public static void SafeAdd<T>(this List<T> list, T item)
    {
        if (!list.Contains(item))
            list.Add(item);
    }

    public static void SafeRemove<T>(this List<T> list, T item)
    {
        if (list.Contains(item))
            list.Remove(item);
    }
}
