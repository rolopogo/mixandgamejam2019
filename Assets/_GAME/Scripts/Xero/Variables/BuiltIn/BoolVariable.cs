﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Xero.Variables
{
    [Serializable, CreateAssetMenu(fileName = "New Bool Variable", menuName = "Variable/Built-In/Bool")]
    public class BoolVariable : VariableObject<bool> { }

    [Serializable]
    public class BoolRef : VariableReference<bool, BoolVariable>
    {
        protected BoolRef(bool value) : base(value) { }
        public static implicit operator BoolRef(Ref r) { return new BoolRef((bool)r.Value); }
    }
}
