﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Xero.Variables
{
    [Serializable, CreateAssetMenu(fileName = "New Double Variable", menuName = "Variable/Built-In/Double")]
    public class DoubleVariable : VariableObject<double> { }

    [Serializable]
    public class DoubleRef : VariableReference<double, DoubleVariable>
    {
        protected DoubleRef(double value) : base(value) { }
        public static implicit operator DoubleRef(Ref r) { return new DoubleRef((double)r.Value); }
    }
}