﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Xero.Variables
{
    [Serializable, CreateAssetMenu(fileName = "New Float Variable", menuName = "Variable/Built-In/Float")]
    public class FloatVariable : VariableObject<float> { }

    [Serializable]
    public class FloatRef : VariableReference<float, FloatVariable>
    {
        protected FloatRef(float value) : base(value) { }
        public static implicit operator FloatRef(Ref r) { return new FloatRef((float)r.Value); }
    }
}
