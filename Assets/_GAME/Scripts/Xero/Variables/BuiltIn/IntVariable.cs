﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Xero.Variables
{
    [Serializable, CreateAssetMenu(fileName = "New Int Variable", menuName = "Variable/Built-In/Int")]
    public class IntVariable : VariableObject<int> { }

    [Serializable]
    public class IntRef : VariableReference<int, IntVariable>
    {
        protected IntRef(int value) : base(value) { }
        public static implicit operator IntRef(Ref r) { return new IntRef((int)r.Value); }
    }
}
