﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Xero.Variables
{
    [Serializable, CreateAssetMenu(fileName = "New String Variable", menuName = "Variable/Built-In/String")]
    public class StringVariable : VariableObject<string> { }

    [Serializable]
    public class StringRef : VariableReference<string, StringVariable>
    {
        protected StringRef(string value) : base(value) { }
        public static implicit operator StringRef(Ref r) { return new StringRef((string)r.Value); }
    }
}
