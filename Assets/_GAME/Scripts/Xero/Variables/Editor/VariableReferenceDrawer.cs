﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEditor;
using UnityEngine;

namespace Xero.Variables.Editor
{
    [CustomPropertyDrawer(typeof(VariableReference), true)]
    public class VariableReferenceDrawer : PropertyDrawer
    {
        dynamic varRef;

        //private static void ApplyIcon(MonoScript script, Texture2D icon)
        //{
        //    PropertyInfo inspectorModeInfo = typeof(SerializedObject).GetProperty("inspectorMode", BindingFlags.NonPublic | BindingFlags.Instance);
        //    inspectorModeInfo.SetValue(serializedObject, InspectorMode.Debug, null);
        //    //SerializedObject serializedObject = new SerializedObject(script);

        //    SerializedProperty iconProperty = serializedObject.FindProperty("m_Icon");
        //    iconProperty.objectReferenceValue = icon;
        //    serializedObject.ApplyModifiedProperties();
        //    serializedObject.Update();
        //    EditorUtility.SetDirty(script);
        //    Debug.Log("Applied Fancy Icon to: " + script.name);
        //}

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            var height = base.GetPropertyHeight(property, label);

            var useConstant = property.FindPropertyRelative("useConstant").boolValue;

            var variable = property.FindPropertyRelative("variable").objectReferenceValue;

            if (!useConstant && variable)
            {
                height += EditorGUIUtility.singleLineHeight;
                height += EditorGUIUtility.standardVerticalSpacing;
            }

            return height;
        }

        //public override VisualElement CreatePropertyGUI(SerializedProperty property)
        //{
        //    // Create property container element.
        //    var container = new VisualElement();

        //    // Create property fields.
        //    //var amountField = new PropertyField(property.FindPropertyRelative("amount"));
        //    //var unitField = new PropertyField(property.FindPropertyRelative("unit"));
        //    //var nameField = new PropertyField(property.FindPropertyRelative("name"), "Fancy Name");
        //    var field = new TextField("Field");

        //    // Add fields to the container.
        //    container.Add(field);

        //    return container;
        //}

        static string[] options =
        {
            "Constant",
            "Variable"
        };

        static GUIStyle dropdown = (GUIStyle)"FoldoutHeaderIcon";
        static GUIStyle labelStyle = (GUIStyle)"flow varPin in";
        static GUIContent valueLabel = new GUIContent("Value");
        public static GUIContent varIcon = EditorGUIUtility.IconContent("FilterByType");
        static float valueLabelSize = dropdown.CalcSize(valueLabel).x;

        static GUIStyle valueBgStyle = (GUIStyle)"ShurikenModuleTitle";
        static GUIStyle valueBgClipStyle = (GUIStyle)"Tag MenuItem";

        static float indentSize = GUIStyle.none.CalcSize(new GUIContent("    ")).x;
        static float size = dropdown.CalcSize(new GUIContent("Constant")).x;

        private int index;

        // Draw the property inside the given rect
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            labelStyle.fontSize *= 2;
            labelStyle.padding = new RectOffset(10, 0, 0, 0);

            var prefixRect = position;
            prefixRect.position += Vector2.left * 9;
            prefixRect.width += 9;

            //property.serializedObject.Update();

            if (varRef is null)
                varRef = property.serializedObject.targetObject;

            // Using BeginProperty / EndProperty on the parent property means that
            // prefab override logic works on the entire property.
            EditorGUI.BeginProperty(position, label, property);

            //label.text = $"{label.text} (Reference)";

            var posRect = position;

            // Draw label
            posRect = EditorGUI.PrefixLabel(posRect, GUIUtility.GetControlID(FocusType.Passive), label);

            var topLabelRect = new Rect(position.position, new Vector2(position.width - posRect.width, position.height));

            dropdown.clipping = TextClipping.Clip;
            dropdown.contentOffset = Vector2.right * EditorGUIUtility.currentViewWidth;

            // Don't make child fields be indented
            var indent = EditorGUI.indentLevel;
            EditorGUI.indentLevel = 0;

            var useConstant = property.FindPropertyRelative("useConstant");
            var constant = property.FindPropertyRelative("constant");
            var variable = property.FindPropertyRelative("variable");

            //EditorGUI.LabelField(pos, "TADAaAAAAAaa");

            var bUseConstant = useConstant.boolValue;

            var rectHeight = posRect.height;

            if (!bUseConstant && variable.objectReferenceValue != null)
            {
                rectHeight -= EditorGUIUtility.standardVerticalSpacing;
                rectHeight /= 2;
            }

            //// Calculate rects
            var popupRect = new Rect(posRect.x - 20, posRect.y + 3, 15, rectHeight);

            var unitRect = new Rect(posRect.x, posRect.y, posRect.width, rectHeight);

            var valueRect = unitRect;
            valueRect.position -= Vector2.down * (EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing);

            //// Draw fields - passs GUIContent.none to each so they are drawn without labels
            //EditorGUI.PropertyField(amountRect, useConstant, GUIContent.none);

            index = useConstant.boolValue ? 0 : 1;
            index = EditorGUI.Popup(popupRect, index, options, dropdown);

            useConstant.boolValue = index == 0;

            //property.serializedObject.Update();

            //var iconRect = position;
            //iconRect.width = iconRect.height;
            //iconRect.position += Vector2.left * 13;
            //iconRect.size *= 0.85f;

            //EditorGUI.LabelField(iconRect, varIcon);

            if (useConstant.boolValue)
            {
                EditorGUI.PropertyField(unitRect, constant, GUIContent.none);
            }
            else
            {
                EditorGUI.PropertyField(unitRect, variable, GUIContent.none);

                SerializedObject variableObj = default;

                if (variable != null && variable.objectReferenceValue != null)
                {
                    variableObj = new SerializedObject(variable.objectReferenceValue);

                    //PropertyInfo inspectorModeInfo = typeof(SerializedObject).GetProperty("inspectorMode", BindingFlags.NonPublic | BindingFlags.Instance);
                    //inspectorModeInfo.SetValue(variableObj, InspectorMode.Debug, null);
                    //SerializedProperty iconProperty = variableObj.FindProperty("m_Icon");
                    //iconProperty.objectReferenceValue = VariableObject.icon;

                    //variableObj.ApplyModifiedProperties();
                    //variableObj.Update();

                    //EditorUtility.SetDirty(variableObj.targetObject);
                }

                if (variable.objectReferenceValue != null && variableObj != null)
                {
                    var offsetRect = position;
                    offsetRect.position +=
                        Vector2.up * (EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing) +
                        Vector2.right * (indent + 1) * indentSize;

                    offsetRect.height = EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing;

                    var bgRect = offsetRect;
                    bgRect.position += Vector2.left * indentSize * 0.25f;

                    var clipRect = bgRect;
                    clipRect.position += Vector2.up * (EditorGUIUtility.singleLineHeight);

                    //EditorGUI.ProgressBar(bgRect, 0, "");
                    EditorGUI.LabelField(bgRect, GUIContent.none, valueBgStyle);
                    EditorGUI.LabelField(clipRect, GUIContent.none, valueBgClipStyle);

                    EditorGUI.PrefixLabel(offsetRect, GUIUtility.GetControlID(FocusType.Passive), valueLabel);

                    var varProp = variableObj.FindProperty("Value");

                    if (varProp.propertyType == SerializedPropertyType.Vector4)
                        varProp.vector4Value = EditorGUI.Vector4Field(valueRect, "", varProp.vector4Value);
                    else
                        EditorGUI.PropertyField(valueRect, variableObj.FindProperty("Value"), GUIContent.none);

                    variableObj.ApplyModifiedProperties();
                    RepaintInspector(property.serializedObject);
                }
            }
            //EditorGUI.Slider(position, 0.0f, 0.0f, 1.0f);
            //EditorGUI.PropertyField(unitRect, property.FindPropertyRelative("unit"), GUIContent.none);
            //EditorGUI.PropertyField(nameRect, property.FindPropertyRelative("name"), GUIContent.none);

            // Set indent back to what it was
            EditorGUI.indentLevel = indent;

            EditorGUI.EndProperty();

            property.serializedObject.ApplyModifiedProperties();
        }

        UnityEditor.Editor thisEditor;

        void RepaintInspector(SerializedObject baseObject)
        {
            if (baseObject == null) return;


            if (thisEditor == null)
                foreach (var editor in ActiveEditorTracker.sharedTracker.activeEditors)
                    if (editor.serializedObject == baseObject)
                        thisEditor = editor;

            thisEditor?.Repaint();
        }
    }
}
