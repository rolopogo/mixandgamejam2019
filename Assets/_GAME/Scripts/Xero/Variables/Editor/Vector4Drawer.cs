﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEditor;
using UnityEngine;

namespace Xero.Variables.Editor
{
    [CustomPropertyDrawer(typeof(Vector4), true)]
    public class Vector4Drawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            //base.OnGUI(position, property, label);

            //base.OnGUI(position, property, label);
            //EditorGUI.Slider(position, 0.0f, 0.0f, 1.0f);
            EditorGUI.BeginProperty(position, label, property);

            property.vector4Value = EditorGUI.Vector4Field(position, label, property.vector4Value);

            EditorGUI.EndProperty();
        }
    }
}
