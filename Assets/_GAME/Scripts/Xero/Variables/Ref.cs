﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Xero.Variables
{
    public class Ref
    {
        public object Value;
        private static Ref Parse<T>(T value) { return new Ref { Value = value }; }

        public static explicit operator Ref(float      value) { return Parse(value); }
        public static explicit operator Ref(int        value) { return Parse(value); }
        public static explicit operator Ref(double     value) { return Parse(value); }
        public static explicit operator Ref(bool       value) { return Parse(value); }
        public static explicit operator Ref(string     value) { return Parse(value); }
        public static explicit operator Ref(Vector4    value) { return Parse(value); }
        public static explicit operator Ref(Vector3    value) { return Parse(value); }
        public static explicit operator Ref(Vector2    value) { return Parse(value); }
        public static explicit operator Ref(Color      value) { return Parse(value); }
        public static explicit operator Ref(Texture    value) { return Parse(value); }
        public static explicit operator Ref(Material   value) { return Parse(value); }
        public static explicit operator Ref(Shader     value) { return Parse(value); }
        public static explicit operator Ref(GameObject value) { return Parse(value); }
        public static explicit operator Ref(Transform  value) { return Parse(value); }
        public static explicit operator Ref(Object     value) { return Parse(value); }
        public static explicit operator Ref(KeyCode    value) { return Parse(value); }
    }
}