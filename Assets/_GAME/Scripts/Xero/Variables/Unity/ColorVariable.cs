﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Xero.Variables
{
    [Serializable, CreateAssetMenu(fileName = "New Color Variable", menuName = "Variable/Unity/Color")]
    public class ColorVariable : VariableObject<Color> { }

    [Serializable]
    public class ColorRef : VariableReference<Color, ColorVariable>
    {
        protected ColorRef(Color value) : base(value) { }
        public static implicit operator ColorRef(Ref r) { return new ColorRef((Color)r.Value); }
    }
}
