﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Xero.Variables
{
    [Serializable, CreateAssetMenu(fileName = "New GameObject Variable", menuName = "Variable/Unity/GameObject")]
    public class GameObjectVariable : VariableObject<GameObject> { }

    [Serializable]
    public class GameObjectRef : VariableReference<GameObject, GameObjectVariable>
    {
        protected GameObjectRef(GameObject value) : base(value) { }
        public static implicit operator GameObjectRef(Ref r) { return new GameObjectRef((GameObject)r.Value); }
    }
}