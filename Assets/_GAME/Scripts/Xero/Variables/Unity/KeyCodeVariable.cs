﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Xero.Variables
{
    [Serializable, CreateAssetMenu(fileName = "New KeyCode Variable", menuName = "Variable/Unity/KeyCode")]
    public class KeyCodeVariable : VariableObject<KeyCode> { }

    [Serializable]
    public class KeyCodeRef : VariableReference<KeyCode, KeyCodeVariable>
    {
        protected KeyCodeRef(KeyCode value) : base(value) { }
        public static implicit operator KeyCodeRef(Ref r) { return new KeyCodeRef((KeyCode)r.Value); }
    }
}
