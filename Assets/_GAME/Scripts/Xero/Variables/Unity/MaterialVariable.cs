﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Xero.Variables
{
    [Serializable, CreateAssetMenu(fileName = "New Material Variable", menuName = "Variable/Unity/Material")]
    public class MaterialVariable : VariableObject<Material> { }

    [Serializable]
    public class MaterialRef : VariableReference<Material, MaterialVariable>
    {
        protected MaterialRef(Material value) : base(value) { }
        public static implicit operator MaterialRef(Ref r) { return new MaterialRef((Material)r.Value); }
    }
}
