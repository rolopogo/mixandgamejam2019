﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Xero.Variables
{
    [Serializable, CreateAssetMenu(fileName = "New Object Variable", menuName = "Variable/Unity/Object")]
    public class ObjectVariable : VariableObject<Object> { }

    [Serializable]
    public class ObjectRef : VariableReference<Object, ObjectVariable>
    {
        protected ObjectRef(Object value) : base(value) { }
        public static implicit operator ObjectRef(Ref r) { return new ObjectRef((Object)r.Value); }
    }
}
