﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Xero.Variables
{
    [Serializable, CreateAssetMenu(fileName = "New Shader Variable", menuName = "Variable/Unity/Shader")]
    public class ShaderVariable : VariableObject<Shader> { }

    [Serializable]
    public class ShaderRef : VariableReference<Shader, ShaderVariable>
    {
        protected ShaderRef(Shader value) : base(value) { }
        public static implicit operator ShaderRef(Ref r) { return new ShaderRef((Shader)r.Value); }
    }
}
