﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Xero.Variables
{
    [Serializable, CreateAssetMenu(fileName = "New Texture Variable", menuName = "Variable/Unity/Texture")]
    public class TextureVariable : VariableObject<Texture> { }

    [Serializable]
    public class TextureRef : VariableReference<Texture, TextureVariable>
    {
        protected TextureRef(Texture value) : base(value) { }
        public static implicit operator TextureRef(Ref r) { return new TextureRef((Texture)r.Value); }
    }
}