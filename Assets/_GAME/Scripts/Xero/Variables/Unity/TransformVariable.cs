﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Xero.Variables
{
    [Serializable, CreateAssetMenu(fileName = "New Transform Variable", menuName = "Variable/Unity/Transform")]
    public class TransformVariable : VariableObject<Transform> { }

    [Serializable]
    public class TransformRef : VariableReference<Transform, TransformVariable>
    {
        protected TransformRef(Transform value) : base(value) { }
        public static implicit operator TransformRef(Ref r) { return new TransformRef((Transform)r.Value); }
    }
}
