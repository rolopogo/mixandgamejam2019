﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Xero.Variables
{
    [Serializable, CreateAssetMenu(fileName = "New Vector2 Variable", menuName = "Variable/Unity/Vector2")]
    public class Vector2Variable : VariableObject<Vector2> { }

    [Serializable]
    public class Vector2Ref : VariableReference<Vector2, Vector2Variable>
    {
        protected Vector2Ref(Vector2 value) : base(value) { }
        public static implicit operator Vector2Ref(Ref r) { return new Vector2Ref((Vector2)r.Value); }
    }
}