﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Xero.Variables
{
    [Serializable, CreateAssetMenu(fileName = "New Vector3 Variable", menuName = "Variable/Unity/Vector3")]
    public class Vector3Variable : VariableObject<Vector3> { }

    [Serializable]
    public class Vector3Ref : VariableReference<Vector3, Vector3Variable>
    {
        protected Vector3Ref(Vector3 value) : base(value) { }
        public static implicit operator Vector3Ref(Ref r) { return new Vector3Ref((Vector3)r.Value); }
    }
}