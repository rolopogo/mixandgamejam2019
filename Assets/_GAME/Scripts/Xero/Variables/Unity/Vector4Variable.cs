﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Xero.Variables
{
    [Serializable, CreateAssetMenu(fileName = "New Vector4 Variable", menuName = "Variable/Unity/Vector4")]
    public class Vector4Variable : VariableObject<Vector4> { }

    [Serializable]
    public class Vector4Ref : VariableReference<Vector4, Vector4Variable>
    {
        protected Vector4Ref(Vector4 value) : base(value) { }
        public static implicit operator Vector4Ref(Ref r) { return new Vector4Ref((Vector4)r.Value); }
    }
}
