﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Xero.Variables
{
    public class VariableObject : ScriptableObject { }

    [Serializable]
    public class VariableObject<T> : VariableObject
    {
        public T Value;

        public static implicit operator T(VariableObject<T> variable)
        {
            return variable.Value;
        }
    }
}
