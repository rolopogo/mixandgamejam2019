﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Xero.Variables
{
    public class VariableReference
    { }

    [Serializable]
    public class VariableReference<T, V> : VariableReference where V : VariableObject<T>
    {
        public bool useConstant = true;

        protected readonly T initConstant;
        [SerializeField] protected T constant;
        [SerializeField] protected V variable;

        public event Action onValueChanged;

        protected VariableReference() { }

        ~VariableReference()
        {
            onValueChanged = null;
        }

        protected VariableReference(T value)
        {
            initConstant = value;
            Value = initConstant;
        }

        public void Set(T value)
        {
            Value = value;
        }

        public T Value
        {
            get
            {
                if (useConstant)
                    return constant;

                if (variable != null)
                    return variable.Value;
                else
                    return default;
            }

            set
            {
                if (Value != null && Value.Equals(value))
                    return;

                if (useConstant)
                    constant = value;
                else if (variable != null)
                    variable.Value = value;

                onValueChanged?.Invoke();
            }
        }

        public static implicit operator T(VariableReference<T, V> vref)
        {
            return vref.Value;
        }
    }
}
